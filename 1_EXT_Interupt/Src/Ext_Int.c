/*
 * Ext_Int.c
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */
#include "Includes.h"

void ExtI_Init(void){
	RCC->APB2ENR |= BV(EXTI_SYSCFG_EN);

	SYSCFG->EXTICR[0] &= ~(EXTICR1_EN);

	EXTI->IMR |= BV(EXTI_UnMsk);

	//EXTI->RTSR |= BV(EXTI_UnMsk);
	EXTI->FTSR |= BV(EXTI_UnMsk);

	NVIC_EnableIRQ(EXTI0_NVIC_Position);
}

void EXTI0_IRQHandler(void){
	EXTI->PR |= BV(EXTI_UnMsk); // Ack. Interrupt
	EXTI0_Flag = 1;

}
