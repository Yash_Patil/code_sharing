/*
 * Switch_Int.c
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#include "Includes.h"

void Switch_Init(void){
	RCC->AHB1ENR |= BV(GPIOA_EN_CLK);

	Switch_GPIO->MODER &= ~(BV(2 * Switch_Pin) | BV(2 * Switch_Pin +1));

	Switch_GPIO->OTYPER &= ~BV(Switch_Pin);

	Switch_GPIO->OSPEEDR &= ~(BV(2 * Switch_Pin) | BV(2 * Switch_Pin +1));

	Switch_GPIO->PUPDR &= ~(BV(2 * Switch_Pin) | BV(2 * Switch_Pin +1));

	ExtI_Init();
}

int Switch_Pressed(){
	if((Switch_GPIO->IDR & BV(Switch_Pin)) != 0) // 0000 0001 & 0000 0001  == 1
		return 1;  // Switch pressed
	else
		return 0;	// Switch not Pressed		 // 0000 0000 & 0000 0001  == 0
}


