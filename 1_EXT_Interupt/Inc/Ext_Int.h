/*
 * Ext_Int.h
 *
 *  Created on: 08-Jul-2021
 *      Author: Franky
 */

#ifndef EXT_INT_H_
#define EXT_INT_H_

#define EXTI_SYSCFG_EN			14
#define EXTICR1_EN				(BV(0) | BV(1) | BV(2) | BV(3))
#define EXTI_UnMsk				0

#define EXTI0_NVIC_Position		6

void ExtI_Init(void);

#endif /* EXT_INT_H_ */
